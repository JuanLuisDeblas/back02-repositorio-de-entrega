package com.techu.backend.controller;


import com.techu.backend.model.ProductoModel;
import com.techu.backend.service.ProductoMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {

    @Autowired
    private ProductoMongoService productoService;

    @GetMapping("")
    public String home() {
        return "API REST Tech U! v2.0.0";
    }

    // GET todos los productos (collection)
    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {

        return productoService.findAll();
    }

    // POST para crear un producto
    @PostMapping("/productos")
    public ProductoModel postProducto(@RequestBody ProductoModel newProduct) {
        productoService.save(newProduct);
        return newProduct;
    }

    // GET a un unico producto por ID (instancia)
    @GetMapping("/productos/{id}")
    public Optional<ProductoModel> getProductoById(@PathVariable String id) {

        return productoService.findById(id);
    }

    @PutMapping("/productos")
    public void putProducto(@RequestBody ProductoModel productoToUpdate) {
        productoService.save(productoToUpdate);
    }

    @DeleteMapping("/productos")
    public boolean deleteProducto(@RequestBody ProductoModel productoToDelete) {
        return productoService.delete(productoToDelete);
    }

}